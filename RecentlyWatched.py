#!/bin/python3
from time import sleep
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options
import sys

class SignIn():
    def Netflix():
        # Netflix Credentials
        if len(sys.argv) != 3:
            username = input("Enter Netflix email: ")
            password = input("Enter Netflix password: ")
        else:
            username = sys.argv[1]
            password = sys.argv[2]
        
        print("Logging in ...")
        # settings for selenium
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        try:
            # initialize the Chrome driver
            driver = webdriver.Chrome(executable_path = '/usr/lib/chromium-browser/chromedriver',options=options)

            # head to Netflix login page
            driver.get("https://Netflix.com/login")
            # find username/email field and send the username itself to the input field
            driver.find_element_by_id("id_userLoginId").send_keys(username)
            # find password input field and insert password as well
            driver.find_element_by_id("id_password").send_keys(password)

            # click login button
            submit = driver.find_element_by_css_selector(".btn.login-button.btn-submit.btn-small")
            submit.click()
            sleep(1)
            print("Successfully logged in")
            # list profiles
            profiles = driver.find_elements_by_class_name("profile-name")
            for name in range(len(profiles)):
                print(name," ",profiles[name].text)
            # chooose profile
            selection = input("Choose corresponding number to choose profile: ")
            profiles[int(selection)].click()
            sleep(1)
            # go to recently watched
            driver.get("https://Netflix.com/viewingactivity")
            sleep(1)
            # select ratings
            ratings = driver.find_element_by_css_selector("#appMountPoint > div > div > div.bd > div > div > div.profile-hub-header > nav > a")
            ratings.click()
            sleep(1)
            # find titles
            titles = []
            # get names of all recent likes
            namelist = driver.find_elements_by_css_selector("#appMountPoint > div > div > div.bd > div > div > ul")

            # store names of all titles in list
            templist = namelist[0].text.split("\n")
            for name in templist:
                try:
                    titles.append(name.split(" ",1)[1])
                # ignore if not a title
                except IndexError:
                    pass
            netflix_list = []
            # try to read from file
            try:
                with open("netflix_list.txt") as f:
                    netflix_list = f.read().splitlines() 
            # else create if doesn't exist
            except FileNotFoundError:
                pass


            f = open("netflix_list.txt","a+")
            for x in titles:
                if x not in netflix_list:
                    f.write(x+'\n')
            f.close()

        finally:
            driver.quit()

if __name__ == '__main__':
    SignIn.Netflix()